<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppBaseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AppBaseController::class, 'index'])->name('Transaksi');
Route::get('/Dashboard', [AppBaseController::class, 'index'])->name('Transaksi');
Route::get('/Transaksi', [AppBaseController::class, 'index'])->name('Transaksi');
Route::Get('Barang', [AppBaseController::class, 'barangindex'])->name('Barang');

Route::Get('ListBarang', [AppBaseController::class, 'ListBarang'])->name('ListBarang');
Route::Post('DeleteBarang', [AppBaseController::class, 'DeleteBarang'])->name('DeleteBarang');
Route::Post('CreateBarang', [AppBaseController::class, 'CreateBarang'])->name('CreateBarang');
Route::Post('EditBarang', [AppBaseController::class, 'EditBarang'])->name('EditBarang');

Route::Post('DeletePerusahaan', [AppBaseController::class, 'DeletePerusahaan'])->name('DeletePerusahaan');
Route::Post('CreatePerusahaan', [AppBaseController::class, 'CreatePerusahaan'])->name('CreatePerusahaan');
Route::Post('EditPerusahaan', [AppBaseController::class, 'EditPerusahaan'])->name('EditPerusahaan');

Route::Post('DeleteTransaksi', [AppBaseController::class, 'DeleteTransaksi'])->name('DeleteTransaksi');
Route::Post('CreateTransaksi', [AppBaseController::class, 'CreateTransaksi'])->name('CreateTransaksi');
Route::Post('EditTransaksi', [AppBaseController::class, 'EditTransaksi'])->name('EditTransaksi');

Route::get('Perusahaan', [AppBaseController::class, 'Perusahaan'])->name('Perusahaan');
Route::Get('ListPerusahaan', [AppBaseController::class, 'ListPerusahaan'])->name('ListPerusahaan');

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
