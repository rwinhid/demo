<!DOCTYPE html>
<html>
<head>
    <title>Laravel 8|7 Datatables Tutorial</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Datatables CSS -->
    <link href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/select/1.3.4/css/select.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
    
<div class="container mt-5">
    <h2 class="mb-4">Laravel 7|8 Yajra Datatables Example</h2>
    <table class="table table-striped table-bordered dt-responsive nowrap yajra-datatable">
        <thead>
            <tr>
                <th class="CheckTitle">All</th>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Username</th>
                <th>Phone</th>
                <th>DOB</th>
                <th>Show</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<!-- Modal PopUp -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Recipient:</label>
            <input type="text" class="form-control" id="recipient-name">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="message-text"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div>
    </div>
  </div>
</div>
</body>
<!-- Jquery Js 3.5.1 -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>  
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!-- DataTables Jquery 1.11.5 -->
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<!-- DataTables bootstrap 4 ~ 1.11.5 -->
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>

<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/select/1.3.4/js/dataTables.select.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
</script>
<script type="text/javascript">
  $(function () {
    function format ( d ) {
        return '<table cellpadding="5" cellspacing="0" border="0" class="table table-bordered" style="width:100%">'+
            '<tr>'+
                '<td>Full name:</td>'+
                '<td>'+d.name+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Extension number:</td>'+
                '<td>'+d.email+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Extra info:</td>'+
                '<td>And any further details here (images etc)...</td>'+
            '</tr>'+
        '</table>';
    }
    var table = $('.yajra-datatable').DataTable({
        dom: 'Bfrtip',
         fixedColumns:   true,
        buttons: [
            {
                text: '<i class="fa fa-plus"></i> Create',
                className: "btn btn-sm btn-outline-primary",
                action: function (e, dt, node, config) {
                
                 $('#exampleModal').modal('show');
                },
                init: function(api, node, config) {
                $(node).removeClass('dt-button')
                }
            },
            {
                text: '<i class="fa fa-files-o"></i> Edit',
                className: "btn btn-sm btn-outline-primary",
                action: function ( e, dt, node, config ) {
                    alert( 'Button activated' );
                },
                init: function(api, node, config) {
                $(node).removeClass('dt-button')
                }
            },
            {
                text: '<i class="fa fa fa-trash-o"></i> Delete',                
                className: "btn btn-sm btn-outline-primary",
                action: function ( e, dt, node, config ) {
                     var data = table.rows( { selected: true } ).data().toArray();
                 Swal.fire({
                    title: 'Apakah anda ingin hapus?',
                    showDenyButton: true,
                    confirmButtonText: 'Iya',
                    denyButtonText: 'Batal',
                    }).then((result) => {
                       
                        if (result.isConfirmed) {
                            edit(data);
                        } else if (result.isDenied) {
                            Swal.fire('Hapus di batalkan', '', 'info')
                        }
                         table.ajax.reload();
                    });
                },
                init: function(api, node, config) {
                $(node).removeClass('dt-button')
                }
            }
        ],
        ajax: "{{ route('students.list') }}",
        
        columnDefs: [
                 {
                    orderable: true,
                    className: 'select-checkbox',
                    targets: 0,
                    checkboxes: {
                         selectRow: true
                    }
                }
            ],
            select: {
                style: 'multi'
            },
        columns: [
           {
                data: null,
                defaultContent: '',
                targets:0,
                checkboxes:{
                    selectRow:true
                }
            },

            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'username', name: 'username'},
            {data: 'phone', name: 'phone'},
            {data: 'dob', name: 'dob'},
            {
                className:      'dt-control',
                orderable:      false,
                data:           null,
                defaultContent: ''
            },
        ]
    });

    table.on("click", "th.select-checkbox", function() {
        if ($("th.select-checkbox").hasClass("selected")) {
            table.rows().deselect();
            $("th.select-checkbox").removeClass("selected");
        } else {
            table.rows().select();
            $("th.select-checkbox").addClass("selected");
        }
    }).on("select deselect", function() {
        ("Some selection or deselection going on")
        if (table.rows({
                selected: true
            }).count() !== table.rows().count()) {
            $("th.select-checkbox").removeClass("selected");
        } else {
            $("th.select-checkbox").addClass("selected");
        }
    });

  
    $('.yajra-datatable tbody').on('click', 'td.dt-control', function () {
        var tr = $(this).closest('tr');
     
        var row = table.row( tr );
        row.select();
        $("th.select-checkbox").addClass("selected");
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
                table.rows(tr).select();
        }
        else {
            // Open this row

            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
    function edit(d){
                var rows = d.map(item => {
                    const container = [];
                    container.push(item.id);
                    // container[item.id] = item.id;
                    return container;
                });
             
                         $.ajax({
                            url: '{{ route("DeleteStudent") }}',
                            type: 'post',
                            data: {data:rows},
                            success: function(response){
                               Swal.fire({
                                icon: 'success',
                                title: response.pesan,
                                showConfirmButton: false,
                                timer: 1500
                                });
                                table.ajax.reload();
                            },
                            error:function(response){
                            Swal.fire({
                                icon: 'error',
                                title: response.pesan,
                                showConfirmButton: false,
                                timer: 1500
                            });
                              console.log(response);
                            }
                        });
                        
                        // alert(data);

    }


  });
</script>
</html>