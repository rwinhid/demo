<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Builder;
use DataTables;
use Auth;
use Carbon\Carbon;
use App\Models\Barang;
use App\Models\Transaksi;
use App\Models\Perusahaan;


class AppBaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function ListBarang(Builder $builder){
    $model = Barang::all();
    return response()->json($model);
    }
    public function ListPerusahaan(Builder $builder){
    $model = Perusahaan::all();
    return response()->json($model);
    }
    
    public function index(Builder $builder)
    {
     if (request()->ajax()) {
           $model = Transaksi::join('barang', 'barang.barang_id', '=', 'transaksi.barang_id')
              		->join('perusahaan', 'perusahaan.perusahaan_id', '=', 'transaksi.perusahaan_id')
                      ->select('barang.title as barang', 'perusahaan.title','transaksi.transaksi_id', 'transaksi.qty','transaksi.created_at as tgl')
              		->get();
        return Datatables::of($model)->make(true);
    }
             $html = $builder->columns([
                ['data' => null,'checkboxes'=>['selectRow'=>true],'orderable'=>false, 'defaultContent'=>'', 'title'=>'All','footer' => 'All'],
                ['data' => 'transaksi_id', 'name'=>'transaksi_id','title'=>'Transaksi Id','footer' => 'Transaksi Id'],
                ['data' => 'barang', 'name'=>'barang','title'=>'Barang','footer' => 'Barang'],
                ['data' => 'qty', 'name'=>'qty','title'=>'Qty','footer' => 'Qty'],
                ['data' => 'tgl', 'name'=>'tgl','title'=>'Tgl Order','footer' => 'Tgl Order'],
                ['data' => null, 'className'=>'dt-control','orderable'=>false,'defaultContent' => '','title'=>'show','footer'=>'show']
            ])->parameters([
                 'searching'=> true,
                'dom'=> 'Bfrtip',
                 'fixedColumns'=>   true,
                'columnDefs' => [
                    ['orderable' => true, 
                     'className' => 'select-checkbox',
                    'targets' => 0,
                    'select-checkbox'=>['selectRow'=>true]]
                ],
                'select'=>['style'=> 'multi']
                ]);

    // return response()->json($posts);
    return view('index', compact('html'));
    }

    public function Perusahaan(Builder $builder)
    {
          if (request()->ajax()) {
         $model = Perusahaan::query();
        return Datatables::of($model)->make(true);
    }
             $html = $builder->columns([
                ['data' => null,'checkboxes'=>['selectRow'=>true],'orderable'=>false, 'defaultContent'=>'', 'title'=>'All','footer' => 'All'],
                ['data' => 'title', 'name'=>'title','title'=>'Nama Perusahaan','footer' => 'Nama Perusahaan'],
                ['data' => 'alamat', 'name'=>'alamat','title'=>'Alamat','footer' => 'Alamat'],
                ['data' => null, 'className'=>'dt-control','orderable'=>false,'defaultContent' => '','title'=>'show','footer'=>'show']
            ])->parameters([
                 'searching'=> true,
                'dom'=> 'Bfrtip',
                 'fixedColumns'=>   true,
                'columnDefs' => [
                    ['orderable' => true, 
                     'className' => 'select-checkbox',
                    'targets' => 0,
                    'select-checkbox'=>['selectRow'=>true]]
                ],
                'select'=>['style'=> 'multi']
                ]);

    // return response()->json($html);
    return view('perusahaan', compact('html'));
    }
    public function barangindex(Builder $builder)
    {
     if (request()->ajax()) {
         $model = Barang::query();
        return Datatables::of($model)->make(true);
    }
             $html = $builder->columns([
                ['data' => null,'checkboxes'=>['selectRow'=>true],'orderable'=>false, 'defaultContent'=>'', 'title'=>'All','footer' => 'All'],
                ['data' => 'title', 'name'=>'title','title'=>'Nama Barang','footer' => 'Nama Barang'],
                ['data' => 'harga', 'name'=>'harga','title'=>'Harga','footer' => 'Harga'],
                ['data' => 'jumlah', 'name'=>'jumlah','title'=>'Jumlah','footer' => 'Jumlah'],
                ['data' => 'created_at', 'name'=>'created_at','title'=>'Tgl','footer' => 'Tgl'],
                ['data' => null, 'className'=>'dt-control','orderable'=>false,'defaultContent' => '','title'=>'show','footer'=>'show']
            ])->parameters([
                 'searching'=> true,
                'dom'=> 'Bfrtip',
                 'fixedColumns'=>   true,
                'columnDefs' => [
                    ['orderable' => true, 
                     'className' => 'select-checkbox',
                    'targets' => 0,
                    'select-checkbox'=>['selectRow'=>true]]
                ],
                'select'=>['style'=> 'multi']
                ]);

    // return response()->json($html);
    return view('barang', compact('html'));
    }

    public function DeleteBarang(Request $request){
        // $data = json_decode($request->data);
        try{
            $delete = Barang::whereIn('barang_id', $request->data);
            $delete->delete();
            $ret = array("status"=>'success',"pesan"=>"Hapus Berhasil");
        } catch (Throwable $e) {
            $ret = array("status"=>'error',"pesan"=>$e);
        }
        return response()->json($ret);
    }
    public function CreateBarang(Request $request){
        
        if(empty($request->Id) ){
           
        $barang = new Barang; 
        $barang->title = $request->nama_barang;
        $barang->harga = $request->harga;
        $barang->jumlah = $request->jumlah;
        $barang->save();
        $ret = array("status"=>'success',"pesan"=>"Create Berhasil");
      
        }else{
             Barang::where('barang_id', $request->Id)->update(['title' => $request->nama_barang,
             'harga'=>$request->harga,'jumlah'=>$request->jumlah]);
            $ret = array("status"=>'success',"pesan"=>"Edit Berhasil");
           
        }
  return response()->json($ret);
    }
     public function CreatePerusahaan(Request $request){
        
        if(empty($request->Id) ){
           
        $barang = new Perusahaan; 
        $barang->title = $request->title;
        $barang->alamat = $request->alamat;
        $barang->save();
        $ret = array("status"=>'success',"pesan"=>"Create Berhasil");
      
        }else{
             Perusahaan::where('perusahaan_id', $request->Id)->update(['title' => $request->title,
             'alamat'=>$request->alamat]);
            $ret = array("status"=>'success',"pesan"=>"Edit Berhasil");
           
        }
  return response()->json($ret);
    }
    public function EditBarang(Request $request){
        try{
            $ret = Barang::where('barang_id', $request->id)->first();
        } catch (Throwable $e) {
            $ret = array("status"=>400,"pesan"=>$e);
        }
        return response()->json($request);
    }
    public function EditPerusahaan(Request $request){
        try{
            $ret = Perusahaan::where('perusahaan_id', $request->id)->first();
        } catch (Throwable $e) {
            $ret = array("status"=>400,"pesan"=>$e);
        }
        return response()->json($request);
    }
      public function DeleteTransaksi(Request $request){
        // $data = json_decode($request->data);
        try{
            $delete = Transaksi::whereIn('transaksi_id', $request->data);
            $delete->delete();
            $ret = array("status"=>'success',"pesan"=>"Hapus Berhasil");
        } catch (Throwable $e) {
            $ret = array("status"=>'error',"pesan"=>$e);
        }
        return response()->json($ret);
    }
    public function DeletePerusahaan(Request $request){
        // $data = json_decode($request->data);
        try{
            $delete = Perusahaan::whereIn('perusahaan_id', $request->data);
            $delete->delete();
            $ret = array("status"=>'success',"pesan"=>"Hapus Berhasil");
        } catch (Throwable $e) {
            $ret = array("status"=>'error',"pesan"=>$e);
        }
        return response()->json($ret);
    }
    public function CreateTransaksi(Request $request)
    {
        $stok = Barang::where('barang_id', $request->barang_id)->first();
        if($stok->jumlah <= $request->qty){
            $data = array("status"=>'error',"pesan"=>"Stok kurang");
             return response()->json($data);
        }else{

       
        // decrement('jumlah',$request->qty);
        // return response()->json($stok);
        
        $digits = 5;
        $date = Carbon::now()->format('Y');

 
        $inv = "INV/".rand($digits,99)."/".$date;

        //  return response()->json($inv);
        if(empty($request->Id) ){
           
        $transaksi = new Transaksi;
        $transaksi->transaksi_id = $inv; 
         $transaksi->user_id = Auth::user()->id;
        $transaksi->perusahaan_id = $request->perusahaan_id;
        $transaksi->barang_id = $request->barang_id;
        $transaksi->qty = $request->qty;
        $transaksi->save();
       Barang::where('barang_id', $request->barang_id)->decrement('jumlah',$request->qty);
        $ret = array("status"=>'success',"pesan"=>"Create Berhasil");
      
        }else{
             Transaksi::where('transaksi_id', $request->Id)->update(['perusahaan_id' => $request->perusahaan_id,
             'barang_id'=>$request->barang_id,'qty'=>$request->qty]);
            $ret = array("status"=>'success',"pesan"=>"Edit Berhasil");
           
        }
    }
        return response()->json($ret);
    }
    public function EditTransaksi(Request $request)
    {

        try{
            $ret = Transaksi::where('transaksi_id', $request->id)->first();
        } catch (Throwable $e) {
            $ret = array("status"=>'error',"pesan"=>$e);
        }
        return response()->json($request);
    }

}