<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Perusahaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('perusahaan', function (Blueprint $table) {
            $table->increments('perusahaan_id');
            $table->string('title');
            $table->string('alamat');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::table('perusahaan')->insert([
            ['title' => 'PT Maju Lancar','alamat'=> 'Jl Alamlain No 9 A'],
            ['title' => 'PT Bintang Jaya','alamat'=> 'Jl Alamlain No 9 A'],
            ['title' => 'PT Sukses Selalu','alamat'=> 'Jl Alamlain No 9 A'],
            ['title' => 'PT Sahabat','alamat'=> 'Jl Alamlain No 9 A'],
            ['title' => 'PT Saudara','alamat'=> 'Jl Alamlain No 9 A']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perusahaan');
        //
    }
};
