<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Barang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang', function (Blueprint $table) {
            $table->increments('barang_id');
            $table->string('title');
            $table->string('harga');
            $table->string('jumlah');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::table('barang')->insert([
            ['title' => 'Meja','harga'=>'50000','jumlah'=>'30'],
            ['title' => 'Kursi','harga'=>'50000','jumlah'=>'30'],
            ['title' => 'Lampu','harga'=>'50000','jumlah'=>'30'],
            ['title' => 'Radio','harga'=>'50000','jumlah'=>'30'],
            ['title' => 'Lemari','harga'=>'50000','jumlah'=>'30'],
            ['title' => 'Kompor','harga'=>'50000','jumlah'=>'30'],
            ['title' => 'Kaca','harga'=>'50000','jumlah'=>'30']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
       Schema::dropIfExists('barang');

    }
}
